import {Heading} from '@chakra-ui/react';

const App = () => {
  return (
    <div>
      <Heading as={'h1'} size={'xl'}>Hello World</Heading>
    </div>
  );
};

export default App;
